﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OxxoPrintService
{
    class TextToPrint
    {
        private const int PaperWidth = 276;

        public String Text { get; set; }
        public Font LineFont { get; set; }
        public Char TextAlign { get; set; }

        public TextToPrint(String text, Font lineFont, Char textAlign)
        {
            Text = text;
            LineFont = lineFont;
            TextAlign = textAlign;
        }

        internal void DrawString(Graphics graphics, float yPos)
        {
            var size = graphics.MeasureString(Text, LineFont);

            switch (TextAlign)
            {
                case 'L':
                    graphics.DrawString(Text, LineFont, Brushes.Black, 0, yPos, new StringFormat());
                    break;
                case '4':
                    graphics.DrawString(Text, LineFont, Brushes.Black, 0.77F * PaperWidth - size.Width, yPos, new StringFormat());
                    break;
                case 'C':
                    graphics.DrawString(Text, LineFont, Brushes.Black, (PaperWidth - size.Width) / 2, yPos, new StringFormat());
                    break;
                case 'R':
                    graphics.DrawString(Text, LineFont, Brushes.Black, PaperWidth - size.Width, yPos, new StringFormat());
                    break;
            }
        }
    }

    class LineToPrint
    {
        public LineToPrint()
        {
            Texts = new List<TextToPrint>();
        }

        public LineToPrint(String text, Font lineFont, Char textAlign) : this()
        {
            Texts.Add(new TextToPrint(text, lineFont, textAlign));
        }

        public List<TextToPrint> Texts { get; set; }
    }

    public class PrintManager
    {
        private List<LineToPrint> invoice;
        private Font printFont;
        private Font printFontBig;
        private Font printFontBigTitle;

        public PrintManager()
        {
            printFont = new Font("Arial", 9);
            printFontBig = new Font("Arial", 11, FontStyle.Bold);
            printFontBigTitle = new Font("Arial", 18, FontStyle.Bold);


        }

        public void PrintInvoice(JObject data)
        {
            DateTime localDate = DateTime.Now;
            var showPreview = Convert.ToBoolean(ConfigurationManager.AppSettings["ShowPreview"]);
            var deliveryValue = Convert.ToDecimal(ConfigurationManager.AppSettings["DeliveryValue"]);
            var total = new Decimal(0);

            invoice = new List<LineToPrint>();

            invoice.Add(new LineToPrint("PANORAMAX", printFontBigTitle, 'C'));
            invoice.Add(new LineToPrint("", printFont, 'L'));

            invoice.Add(new LineToPrint("SOLICITUD EXAMENES", printFontBig, 'C'));
            invoice.Add(new LineToPrint("", printFont, 'L'));

            var gridTitleLine5 = new LineToPrint();
            gridTitleLine5.Texts.Add(new TextToPrint("FECHA :", printFont, 'L'));
            gridTitleLine5.Texts.Add(new TextToPrint(String.Format("{0:dd/MM/yyyy hh:mm:ss tt}", localDate.AddHours(-5)), printFont, 'R'));
           // invoice.Add(new LineToPrint(String.Format("Fecha: {0:dd/MM/yyyy hh:mm:ss tt}", data["Fecha"].Value<DateTime>().AddHours(-5)), printFont, 'L'));

            invoice.Add(gridTitleLine5);

            invoice.Add(new LineToPrint(String.Format("PACIENTE: {0}","               "+data["pedido"]["PacienteNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("DOCTOR: {0}","                 "+data["pedido"]["DoctorNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("TIPO SERVICIO: {0}", "      "+data["pedido"]["TipoServicioNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("OBSERVACIONES: {0}", " "+data["pedido"]["observaciones"].Value<String>()), printFont, 'L'));

           /* var gridTitleLine = new LineToPrint();
            gridTitleLine.Texts.Add(new TextToPrint("PACIENTE:", printFont, 'L'));
            gridTitleLine.Texts.Add(new TextToPrint(String.Format(data["pedido"]["PacienteNombre"].Value<String>()), printFont, 'R'));
            invoice.Add(gridTitleLine);

            var gridTitleLine2 = new LineToPrint();
            gridTitleLine2.Texts.Add(new TextToPrint("DOCTOR:", printFont, 'L'));
            gridTitleLine2.Texts.Add(new TextToPrint(String.Format(data["pedido"]["DoctorNombre"].Value<String>()), printFont, 'R'));
            invoice.Add(gridTitleLine2);

            var gridTitleLine3 = new LineToPrint();
            gridTitleLine3.Texts.Add(new TextToPrint("TIPO SERVICIO:", printFont, 'L'));
            gridTitleLine3.Texts.Add(new TextToPrint(String.Format(data["pedido"]["TipoServicioNombre"].Value<String>()), printFont, 'R'));
            invoice.Add(gridTitleLine3);

            var gridTitleLine4 = new LineToPrint();
            gridTitleLine4.Texts.Add(new TextToPrint("OBSERVACIONES:", printFont, 'L'));
            gridTitleLine4.Texts.Add(new TextToPrint(String.Format(data["pedido"]["observaciones"].Value<String>()), printFont, 'R'));
            invoice.Add(gridTitleLine4);*/

           /* invoice.Add(new LineToPrint(String.Format("PACIENTE: {0}", data["pedido"]["PacienteNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("DOCTOR: {0}", data["pedido"]["DoctorNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("TIPO SERVICIO: {0}", data["pedido"]["TipoServicioNombre"].Value<String>()), printFont, 'L'));
            invoice.Add(new LineToPrint(String.Format("OBSERVACIONES: {0}", data["pedido"]["observaciones"].Value<String>()), printFont, 'L'));*/
            invoice.Add(new LineToPrint("", printFont, 'L'));

            /* invoice.Add(new LineToPrint("--------------PRUEBA--------------------------------------------------------", printFont, 'L'));


             invoice.Add(new LineToPrint(String.Format("DETALLES EXAMENES",
                  data["pedido"]["PacienteNombre"].Value<String>(),
                  data["pedido"]["PacienteNombre"].Value<String>(),
                  data["pedido"]["PacienteNombre"].Value<String>(),
                  data["pedido"]["PacienteNombre"].Value<String>()), printFont, 'L'));
              invoice.Add(new LineToPrint(data["pedido"]["PacienteNombre"].Value<String>(), printFont, 'L'));
              invoice.Add(new LineToPrint("", printFont, 'L'));
              //invoice.Add(new LineToPrint(String.Format("Recibo: {0}", data["Id"].Value<Int32>()), printFont, 'L'));
             // invoice.Add(new LineToPrint(String.Format("Fecha: {0:dd/MM/yyyy hh:mm:ss tt}", data["Fecha"].Value<DateTime>().AddHours(-5)), printFont, 'L'));
              invoice.Add(new LineToPrint("", printFont, 'L'));

              var gridTitleLine = new LineToPrint();

              gridTitleLine.Texts.Add(new TextToPrint("Item", printFont, 'L'));
              gridTitleLine.Texts.Add(new TextToPrint("Precio", printFont, '4'));
              gridTitleLine.Texts.Add(new TextToPrint("Valor", printFont, 'R'));

              invoice.Add(gridTitleLine);
              invoice.Add(new LineToPrint("-----------------FIN PRUEBA-----------------------------------------------------", printFont, 'L'));*/

            invoice.Add(new LineToPrint("----------------------------------------------------------------------", printFont, 'L'));
           
            var catIdant = "";
            foreach (var det in data["detalles"])
            {
                var line = new LineToPrint();
                var line2 = new LineToPrint();
                var line3 = new LineToPrint();
                
             
                var campo = det["campoAbierto"].Value<String>();
                var subcatnom = det["SubCategoriumNombre"].Value<String>();
                
                var catNombre = det["CategoriaNombre"].Value<String>();

                line.Texts.Add(new TextToPrint(catNombre, printFont, 'L'));
                line2.Texts.Add(new TextToPrint("  "+subcatnom, printFont, 'L'));
                line3.Texts.Add(new TextToPrint("    "+campo, printFont, 'L'));



                /*line.Texts.Add(new TextToPrint(String.Format("{0:C0} x {1}", vu, cnt), printFont, '4'));
                line.Texts.Add(new TextToPrint(String.Format("{0:C0}", vu * cnt), printFont, 'R'));*/

                if (catIdant != det["CategoriaId"].Value<String>())
                {
                    invoice.Add(new LineToPrint("", printFont, 'L'));
                    invoice.Add(line);
                    

                }
                    
                invoice.Add(line2);
                if (campo != "ninguno")
                {
                    invoice.Add(line3);
                }

                

                var catgoriaId = det["CategoriaId"].Value<String>();
                catIdant = catgoriaId;



                // total += vu * cnt;
            }

            invoice.Add(new LineToPrint("", printFont, 'L'));
            invoice.Add(new LineToPrint("----------------------------------------------------------------------", printFont, 'L'));

            /* invoice.Add(new LineToPrint("----------------------------------------------------------------------", printFont, 'L'));
             invoice.Add(new LineToPrint(String.Format("Num. Items: {0}", data["DetallePedidos"].Count()), printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("FORMA DE PAGO", printFont, 'C'));
             invoice.Add(new LineToPrint(data["MedioPago"]["Nombre"].Value<String>(), printFont, 'C'));

             var lineSubtotal = new LineToPrint();
             var lineDomicilio = new LineToPrint();
             var lineTotal = new LineToPrint();

             lineSubtotal.Texts.Add(new TextToPrint("Subtotal:", printFontBig, 'L'));
             lineSubtotal.Texts.Add(new TextToPrint(String.Format("{0:C0}", total), printFontBig, 'R'));
             lineDomicilio.Texts.Add(new TextToPrint("Domicilio:", printFontBig, 'L'));
             lineDomicilio.Texts.Add(new TextToPrint(String.Format("{0:C0}", deliveryValue), printFontBig, 'R'));
             lineTotal.Texts.Add(new TextToPrint("Total:", printFontBig, 'L'));
             lineTotal.Texts.Add(new TextToPrint(String.Format("{0:C0}", total + deliveryValue), printFontBig, 'R'));

             invoice.Add(lineSubtotal);
             invoice.Add(lineDomicilio);
             invoice.Add(lineTotal);

             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("", printFont, 'L'));
             invoice.Add(new LineToPrint("Gracias por su compra", printFont, 'C'));*/

            var printDocument = new PrintDocument();
            var paperSize = new PaperSize("Custom", 315, 16535);
            //var paperSize = new PaperSize("Custom", 315, 16535);

            paperSize.RawKind = (int)PaperKind.Custom;

            printDocument.PrintPage += PrintDocument_PrintPage;
            printDocument.DefaultPageSettings.PaperSize = paperSize;
            printDocument.PrinterSettings.DefaultPageSettings.PaperSize = paperSize;

            if (showPreview)
            {
                var printPreview = new PrintPreviewDialog();

                printPreview.Document = printDocument;
                printPreview.ShowDialog();
            }
            else
            {
                printDocument.Print();
            }
        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            var deliveryValue = Convert.ToDecimal(ConfigurationManager.AppSettings["DeliveryValue"]);
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = e.MarginBounds.Left;
            float topMargin = e.MarginBounds.Top;

            leftMargin = 0;
            topMargin = 0;
            // Calculate the number of lines per page.
            linesPerPage = e.MarginBounds.Height / printFont.GetHeight(e.Graphics);

            foreach (var line in invoice)
            {
                yPos = topMargin + (count * line.Texts[0].LineFont.GetHeight(e.Graphics));

                foreach (var item in line.Texts)
                {
                    item.DrawString(e.Graphics, yPos);
                }
                count++;
            }
        }
    }
}
