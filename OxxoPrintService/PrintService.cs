﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;

namespace OxxoPrintService
{
    public partial class PrintService : ServiceBase
    {
        Socket socket;

        public PrintService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var urlEndPoint = ConfigurationManager.AppSettings["UrlEndPoint"];

            socket = IO.Socket(urlEndPoint);
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                //socket.Emit("hi");

            });

            socket.On("new-order", (data) =>
            {
                //Console.WriteLine(data);
                //socket.Disconnect();
                var print = new PrintManager();

                print.PrintInvoice((JObject)data);
            });
            Console.ReadLine();
        }

        protected override void OnStop()
        {
            try
            {
                socket.Disconnect().Close();
            }
            catch { }


        }

        public void Start()
        {
            OnStart(null);
        }
    }
}
