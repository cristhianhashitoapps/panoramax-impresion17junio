﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OxxoPrintServiceTest
{
    public partial class Form1 : Form
    {
        OxxoPrintService.PrintService service;
        public Form1()
        {
            InitializeComponent();

            service = new OxxoPrintService.PrintService();

            service.Start();
        }
    }
}
